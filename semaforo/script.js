document.addEventListener("DOMContentLoaded", function () {
    const lights = document.querySelectorAll(".light");
    let activeLightIndex = 0;
  
    function activateLight(index) {
      lights.forEach((light, i) => {
        if (i === index) {
          light.classList.add("active");
        } else {
          light.classList.remove("active");
        }
      });
    }
  
    function changeLight() {
      activateLight(activeLightIndex);
      activeLightIndex = (activeLightIndex + 1) % lights.length;
    }
  
    setInterval(changeLight, 5000); // Cambiar el color cada 5 segundos (5000 milisegundos)
  });
  